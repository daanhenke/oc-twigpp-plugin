<?php namespace DaanHenke\TwigPP\Twig\Nodes;

use DaanHenke\TwigPP\Twig\Filters\FileFilters;
use Twig\Node\Node;
use Twig\Node\Expression\AbstractExpression;
use Twig\Compiler;

class IconNode extends Node
{
    public static $directory_lookup =
    [
        "fab" => "font-awesome/brands",
        "faduo" => "font-awesome/duotone",
        "fal" => "font-awesome/light",
        "far" => "font-awesome/regular",
        "fas" => "font-awesome/solid"
    ];

    public function __construct(AbstractExpression $icon, ?AbstractExpression $classes, int $line, string $tag = null)
    {
        $nodes = [
            "icon" => $icon
        ];

        if (! is_null($classes))
        {
            $nodes["classes"] = $classes;
        }

        parent::__construct($nodes, [], $line, $tag);
    }

    public function compile(Compiler $compiler)
    {
        $icon = $this->getNode("icon")->getAttribute("value");

        if ($this->hasNode("classes"))
        {
            $classes = $this->getNode("classes")->getAttribute("value");
        }
        else
        {
            $classes = "";
        }

        $svg = FileFilters::icon($icon, $classes);

        if ($svg === null)
        {
            $svg = '<script>alert("Unknown icon: ' . $icon . '")</script>';
        }

        $compiler
            ->addDebugInfo($this)
            ->write("echo '" . $svg . "';");
    }
}
