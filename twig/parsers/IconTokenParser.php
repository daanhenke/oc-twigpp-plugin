<?php namespace DaanHenke\TwigPP\Twig\Parsers;

use Twig\TokenParser\AbstractTokenParser;
use Twig\Token;
use DaanHenke\TwigPP\Twig\Nodes\IconNode;

class IconTokenParser extends AbstractTokenParser
{
    public function parse(Token $token)
    {
        $line_number = $token->getLine();
        $stream = $this->parser->getStream();

        $classes = null;

        $icon = $this->parser->getExpressionParser()->parseExpression();
        if (! $stream->nextIf(Token::BLOCK_END_TYPE))
        {
            $classes = $this->parser->getExpressionParser()->parseExpression();
            $stream->expect(Token::BLOCK_END_TYPE);
        }


        return new IconNode($icon, $classes, $line_number, $this->getTag());
    }

    public function getTag()
    {
        return "icon";
    }
}