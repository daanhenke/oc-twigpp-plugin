<?php namespace DaanHenke\TwigPP\Twig\Filters;

use File;
use Cms\Classes\Theme;

class OctoberFilters
{
    public static function asset(string $url): string
    {
        $theme = Theme::getActiveTheme();
        $assets_uri = "/themes/{$theme->getDirName()}/assets";
        $assets_path = "{$theme->getPath()}/assets";

        if (File::exists("{$assets_path}/dist/manifest.json"))
        {
            $content = json_decode(File::get("{$assets_path}/dist/manifest.json"), true);
            
            if ($content !== null)
            {
                if (array_key_exists($url, $content))
                {
                    return "$assets_uri/dist/{$content[$url]}";
                }
            }
        }

        return "$assets_uri/$url";
    }
}