<?php namespace DaanHenke\TwigPP\Twig\Filters;

class StringFilters
{
    public static function uppercase(string $string): string
    {
        return strtoupper($string);
    }

    public static function lowercase(string $string): string
    {
        return strtolower($string);
    }

    public static function capitalize(string $string, string $mode = 'first'): string
    {
        switch ($mode)
        {
            case 'first':
                return ucfirst($string);
            case 'all':
                return ucwords($string);
        }
    }
}