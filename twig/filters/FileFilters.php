<?php namespace DaanHenke\TwigPP\Twig\Filters;

use Cms\Classes\Theme;
use File;

class FileFilters
{
    public static function icon($name, $classes = "")
    {
        $theme = Theme::getActiveTheme();
        $theme_path = themes_path() . "/{$theme->getDirName()}/icons/";

        if (! File::exists($theme_path))
        {
            mkdir($theme_path);
        }

        $chunks = explode("-", $name);

        if (count($chunks) < 2)
        {
            return;
        }

        $directory = $chunks[0];
        $icon = implode("-", array_splice($chunks, 1));

        if (! File::exists("{$theme_path}/{$directory}"))
        {
            return;
        }

        $path = $theme_path . $directory . "/" . $icon . ".svg";

        if (! File::exists($path))
        {
            return;
        }

        $svg = file_get_contents($path);
        $svg_data = substr($svg, 4);
        $class_string =  "fa" . ($classes === "" ? "" : " " . $classes);

        return '<svg class="' . $class_string . '" aria-hidden="true" role="img" fill="currentColor"' . $svg_data;
    }
}