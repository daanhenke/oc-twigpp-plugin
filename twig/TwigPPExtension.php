<?php namespace DaanHenke\TwigPP\Twig;

use DaanHenke\TwigPP\Twig\Parsers\IconTokenParser;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TwigPPExtension extends AbstractExtension
{
    public function getName()
    {
        return 'TwigPP';
    }

    public function getFilters()
    {
        return [
            // October Filters
            new TwigFilter('asset', ['DaanHenke\TwigPP\Twig\Filters\OctoberFilters', 'asset']),

            // String Filters
            new TwigFilter('uppercase', ['DaanHenke\TwigPP\Twig\Filters\StringFilters', 'uppercase']),
            new TwigFilter('lowercase', ['DaanHenke\TwigPP\Twig\Filters\StringFilters', 'lowercase']),
            new TwigFilter('capitalize', ['DaanHenke\TwigPP\Twig\Filters\StringFilters', 'capitalize']),

            // File Filters
            new TwigFilter('icon', ['DaanHenke\TwigPP\Twig\Filters\FileFilters', 'icon'], [
                'is_safe' => ['html']
            ])
        ];
    }

    public function getTokenParsers()
    {
        return [
            new IconTokenParser()
        ];
    }
}