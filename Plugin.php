<?php namespace DaanHenke\TwigPP;

use Event;
use Cms\Classes\Controller;
use DaanHenke\TwigPP\Twig\TwigPPExtension;
use System\Classes\PluginBase;


class Plugin extends PluginBase
{
    public function boot()
    {
        Event::listen('cms.page.beforeDisplay', function(Controller $controller, $url, $page) {
            $ext = new TwigPPExtension();

            if (! $controller->getTwig()->hasExtension($ext->getName()))
            {
                $controller->getTwig()->addExtension($ext);
            }
        });
    }
}
